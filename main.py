from flask import Flask
import redis

app = Flask(__name__)
r = redis.Redis(host='localhost', port=6379, db=0)

@app.route('/')
def hello():
    count = r.incr('visits')
    return f'Hello World! You have visited this site {count} times.'

if __name__ == '__main__':
    app.run()